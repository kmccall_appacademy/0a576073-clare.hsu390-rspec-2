def reverser
  yield.split.map {|el| el.reverse}.join(" ")
end

def adder(num = 1)
  num + yield
end

def repeater(n=0)
  if n == 0
    return yield
  else
    n.times {|x| yield}
    end
end
